﻿using SJB119.App_Code;
using SJB119.Util;
using System;
using System.Runtime.InteropServices;

namespace SJB119
{
    class SJB119
    {
        [DllImport("Kernel32.dll")]
        public static extern bool ExitProcess(
            int Codigo
        );

        public static string arg { get; set; }

        /// <summary>
        /// Main Method;
        /// </summary>
        /// <param name="args">Parâmetros Control-M</param>
        static void Main(string[] args)
        {
            //if ((args == null) || (args.Length <= 0))
            //    throw new Exception("Informe o parâmetro de execução: 'H' -> Hayamax ou 'S' -> Demais Seller's.");

            arg = args[0];

            Log.GravaLog(" Inicializando o processo SJB119", true, false, arg);

            SJB119BLL BLL = new SJB119BLL();
            bool processadoComSucesso = true;
            DateTime dataInicioLocal = DateTime.Now;
            try {
                if (!((args == null) || (args.Length <= 0))) {
                    Log.GravaLog(" # Processando validação dos preços entre a OpenK e o Martin", false, false, arg);
                    BLL.ValidarPrecoOpenKComMartins();
                } else {
                    Log.GravaLog(" # Processando validação dos preços entre a tabela MRT.CADPCOPRDCTLAPIMKP e a tabela MRT.HSTPCOPRDCTLAPIMKP", false, false, arg);
                    BLL.ValidarPrecoOpenKComMartinsComParemetro(arg);
                }
            }
            catch (Exception ex)
            {
                processadoComSucesso = false;
                Log.GravaLog(" # Erro: " + ex.ToString(), false, false, arg);
            }
            finally
            {
                Log.GravaLog(" # Tempo total gasto: " + DateTime.Now.Subtract(dataInicioLocal).TotalSeconds.ToString(), false, false, arg);
                Log.GravaLog(" # Finalizando Processamento de validação dos preços entre a Openk e o Martins", false, true, arg);
                BLL.Dispose();
                GC.Collect();
            }

            if (processadoComSucesso)
                ExitProcess(0);
            else
                ExitProcess(12);
        }
    }
}
