﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SJB119.Util
{
    class Log
    {
        #region GravaLog
        public static void GravaLog(string str, bool iniciandoLog, bool finalizandoLog, string arg)
        {
            Console.WriteLine(str);
            object _Lock1 = new object();
            lock (_Lock1)
            {
                try
                {
                    bool append = true;
                    if (iniciandoLog && DateTime.Now.Day.Equals(17) && (DateTime.Now.Hour >= 6 & DateTime.Now.Hour < 7))
                    {
                        append = false;
                    }
                    else if (iniciandoLog && (DateTime.Now.Hour >= 6 & DateTime.Now.Hour < 7))
                    {
                        append = false;
                    }

                    string caminho = ConfigurationManager.AppSettings.GetValues("pathLOG").First();
                    if (arg != "")
                        caminho = caminho.Split('.')[0].ToString() + "_" + arg + "." + caminho.Split('.')[1].ToString();
                    else
                        caminho = caminho.Split('.')[0].ToString() + "." + caminho.Split('.')[1].ToString();

                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(caminho, append))
                    {
                        if (iniciandoLog)
                        {
                            file.WriteLine("==============================================================");
                            file.WriteLine(" # Data: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                            file.WriteLine(" # Log: " + str);
                            file.WriteLine("");
                        }
                        else if (finalizandoLog)
                        {
                            file.WriteLine(" # Log: " + str);
                            file.WriteLine(" # Data: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                            file.WriteLine("==============================================================");
                        }
                        else
                        {
                            file.WriteLine(str);
                            file.WriteLine("");
                        }
                        file.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
        #endregion
    }
}
