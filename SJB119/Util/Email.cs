﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Linq;

namespace SJB119.Util
{
    /// <summary>
    /// Classe utilizada para enviar E-Mail;
    /// </summary>
    public class Email
    {
        private List<string> _destinatarios;
        public List<string> Destinatarios {
            get {
                if (_destinatarios == null)
                    _destinatarios = new List<string>();

                return _destinatarios;
            }
            set {
                if (value == null)
                    value = new List<string>();

                _destinatarios = value;
            }
        }

        private List<string> _destinatariosCC;
        public List<string> DestinatariosCC {
            get {
                if (_destinatariosCC == null)
                    _destinatariosCC = new List<string>();

                return _destinatariosCC;
            }
            set {
                if (value == null)
                    value = new List<string>();

                _destinatariosCC = value;
            }
        }

        private List<string> _destinatariosBcc;
        public List<string> DestinatariosBcc {
            get {
                if (_destinatariosBcc == null)
                    _destinatariosBcc = new List<string>();

                return _destinatariosBcc;
            }
            set {
                if (value == null)
                    value = new List<string>();

                _destinatariosBcc = null;
            }
        }
        public string Remetente { get; set; }
        public string Assunto { get; set; }
        public string Mensagem { get; set; }
        public string SmtpClient { get; set; }
        public bool ConteudoHTML { get; set; }

        public void Send()
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient Email = new SmtpClient(SmtpClient);

                foreach (string destinatario in Destinatarios)
                {
                    mail.To.Add(new MailAddress(destinatario));
                }

                foreach (string destinatarioCC in DestinatariosCC)
                {
                    mail.CC.Add(new MailAddress(destinatarioCC));
                }

                foreach (string destinatarioBcc in DestinatariosBcc)
                {
                    mail.Bcc.Add(new MailAddress(destinatarioBcc));
                }

                mail.Body = Mensagem;
                mail.From = new MailAddress(Remetente);
                mail.Subject = Assunto;
                mail.IsBodyHtml = ConteudoHTML;

                Email.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Envia e-mail para destinatário especificado;
        /// </summary>
        /// <param name="eMailTo">Destinatário</param>
        /// <param name="body">Mensagem do corpo do email</param>
        /// <param name="emailAssunto">Assunto</param>
        /// <param name="isBodyHtml">Indica se conteúdo do e-mail é html</param>
        /// <param name="eMailBcc">Destinatário oculto</param>
        public static void EnviaEmail(string eMailTo, string body, string emailAssunto, bool isBodyHtml, string eMailBcc = "")
        {
            try
            {
                MailMessage oEmail = new MailMessage
                {
                    From = new MailAddress(ConfigurationManager.AppSettings.GetValues("Remetente").First())
                };

                foreach (string eMail in eMailTo.Split(';'))
                {
                    if (eMail.Trim().Length > 0)
                        oEmail.To.Add(eMail.Trim());
                }

                foreach (string eMail in eMailBcc.Split(';'))
                {
                    if (eMail.Trim().Length > 0)
                        oEmail.Bcc.Add(eMail.Trim());
                }

                string newBody = string.Empty;
                if (Dns.GetHostName().Trim().ToUpper() == "SERVIDOR_HOMOLOGACAO")
                    newBody = "[HOST - HOMOLOGAÇÃO] " + body;
                else if (Dns.GetHostName().Trim().ToUpper() == "BETRIA")
                    newBody = "[HOST - PRODUÇÃO] " + body;
                else
                    newBody = "[HOST - DESCONHECIDO] " + body;

                oEmail.IsBodyHtml = isBodyHtml;
                oEmail.Body = newBody;
                oEmail.Subject = "SJB119 - Email Automático";

                if ((emailAssunto != null) && (emailAssunto.Length > 0))
                    oEmail.Subject = emailAssunto;

                SmtpClient smtp = new SmtpClient
                {
                    Host = ConfigurationManager.AppSettings.GetValues("Smtp").First()
                };

                smtp.Send(oEmail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}