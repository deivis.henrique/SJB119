﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using SJB119.App_Code.MkpB2B;
using System;
using System.Text;

namespace SJB119.App_Code
{
    public class SJB119DAL : Martins.Layers.DAL
    {
        private Database objDB;
        public DbConnection connection_MRT001;
        public DbTransaction transaction;

        public SJB119DAL()
        {
            SystemConfigurationSource systemSource = new SystemConfigurationSource();
            DatabaseProviderFactory factory = new DatabaseProviderFactory(systemSource);

            objDB = factory.CreateDefault();

            connection_MRT001 = objDB.CreateConnection();
            connection_MRT001.Open();
        }

        public void BeginTransaction()
        {
            transaction = connection_MRT001.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void CommitTransaction()
        {
            if (transaction != null)
            {
                transaction.Commit();
                this.DisposeTransaction();
            }
        }

        public void RollBackTransaction()
        {
            if (transaction != null)
            {
                transaction.Rollback();
                this.DisposeTransaction();
            }
        }

        private void DisposeTransaction()
        {
            if (transaction != null)
            {
                transaction.Dispose();
                transaction = null;
            }
        }


        /// <summary>
        /// Buscar os produtos na tabela MRT.CADPCOPRDCTLAPIMKP
        /// </summary>
        public List<ProductMartins> BuscarProdutosSellerBaseMartins(long seller_id) {
            List<ProductMartins> productMartins = new List<ProductMartins>();
            StringBuilder sqlCmd = new StringBuilder();

            sqlCmd.AppendLine("Select B.DESEMPSRR, A.NUMIDTMERAPIMKP, A.VLRVNDMER, A.DESLSTPCOENVFRN");
            sqlCmd.AppendLine("From MRT.CADPCOPRDCTLAPIMKP A, MRT.CADEMPSRR B");
            sqlCmd.AppendLine("Where B.NUMIDTEMPSRRAPIMKP = A.NUMIDTEMPSRRAPIMKP AND CODEMPSRR > 3 AND A.CADPCOPRDCTLAPIMKP = @IDSELLER");
            sqlCmd.AppendLine("  AND CODEMPSRR > 3");
            sqlCmd.AppendLine("  AND A.NUMIDTEMPSRRAPIMKP = :NUMIDTEMPSRRAPIMKP");

            DbCommand DataBaseCommand = objDB.GetSqlStringCommand(sqlCmd.ToString());

            objDB.AddInParameter(DataBaseCommand, "NUMIDTEMPSRRAPIMKP", DbType.Int64, seller_id);

            IDataReader idr = objDB.ExecuteReader(DataBaseCommand);

            if (idr.Read())
                productMartins.Add( new ProductMartins {
                    DESEMPSRR = idr["DESEMPSRR"].ToString(),
                    NUMIDTMERAPIMKP = idr["NUMIDTMERAPIMKP"].ToString(),
                    VLRVNDMER = idr["VLRVNDMER"].ToString(),
                    DESLSTPCOENVFRN = idr["DESLSTPCOENVFRN"].ToString(),
                });

            return productMartins;
        }

        public List<ProductHistoricoMartins> BuscarProdutosSellerBaseHistorico(long seller_id) {

            List<ProductHistoricoMartins> productHistoricoMartins = new List<ProductHistoricoMartins>();
            StringBuilder sqlCmd = new StringBuilder();

            sqlCmd.AppendLine("Select NUMIDTEMPSRRAPIMKP, VLRVNDMER");
            sqlCmd.AppendLine("From MRT.HSTPCOPRDCTLAPIMKP");
            sqlCmd.AppendLine("Where NUMIDTEMPSRRAPIMKP = :NUMIDTEMPSRRAPIMKP");
            sqlCmd.AppendLine("Order By DATRGT ASC");

            DbCommand DataBaseCommand = objDB.GetSqlStringCommand(sqlCmd.ToString());

            objDB.AddInParameter(DataBaseCommand, "NUMIDTEMPSRRAPIMKP", DbType.Int64, seller_id);

            IDataReader idr = objDB.ExecuteReader(DataBaseCommand);

            if (idr.Read())
                productHistoricoMartins.Add(new ProductHistoricoMartins
                {
                    NUMIDTEMPSRRAPIMKP = idr["NUMIDTEMPSRRAPIMKP"].ToString(),
                    VLRVNDMER = idr["VLRVNDMER"].ToString(),
                });

            return productHistoricoMartins;
        }


        /// <summary>
        /// Verifica se existe produto e retorna a data de inclusão;
        /// </summary>
        public IDataReader DataInclusaobyId(long product_id, long seller_code, string scope)
        {
            string sqlCmd = string.Format(
                @"SELECT * 
                  FROM  MRT.CADPCOPRDCTLAPIMKP p 
                  WHERE p.NUMIDTMERAPIMKP = :NUMIDTMERAPIMKP
                  AND p.NUMIDTEMPSRRAPIMKP = :NUMIDTEMPSRRAPIMKP
                  AND P.DESLSTPCOENVFRN = :DESLSTPCOENVFRN").Replace("\n", "").Replace("\t", "").Replace("\r", "");

            DbCommand DataBaseCommand = objDB.GetSqlStringCommand(sqlCmd);

            objDB.AddInParameter(DataBaseCommand, "NUMIDTMERAPIMKP", DbType.Int64, product_id);
            objDB.AddInParameter(DataBaseCommand, "NUMIDTEMPSRRAPIMKP", DbType.Int64, seller_code);
            objDB.AddInParameter(DataBaseCommand, "DESLSTPCOENVFRN", DbType.String, scope.Trim());

            return objDB.ExecuteReader(DataBaseCommand);
        }

        public void InsertItem(Product product, Price price, string scope)
        {
            string sqlCmd = @"INSERT INTO MRT.CADPCOPRDCTLAPIMKP (
                                 NUMIDTMERAPIMKP,
                                 NUMIDTEMPSRRAPIMKP,
                                 DESLSTPCOENVFRN,
                                 VLRVNDMER,
                                 DATRGT,
                                 DATALT,
                                 FLGENV)
                              VALUES (
                                 :NUMIDTMERAPIMKP,
                                 :NUMIDTEMPSRRAPIMKP,
                                 :DESLSTPCOENVFRN,
                                 :VLRVNDMER,
                                 :DATRGT,
                                 :DATALT,
                                 :FLGENV)";


            DbCommand DataBaseCommand = objDB.GetSqlStringCommand(sqlCmd.ToString());

            objDB.AddInParameter(DataBaseCommand, "NUMIDTMERAPIMKP", DbType.Int64, product.product_id);
            objDB.AddInParameter(DataBaseCommand, "NUMIDTEMPSRRAPIMKP", DbType.Int64, product.seller_code);
            objDB.AddInParameter(DataBaseCommand, "DESLSTPCOENVFRN", DbType.String, scope.Trim());
            objDB.AddInParameter(DataBaseCommand, "VLRVNDMER", DbType.Double, price.sale_price);
            objDB.AddInParameter(DataBaseCommand, "DATRGT", DbType.DateTime, DateTime.Now);
            objDB.AddInParameter(DataBaseCommand, "DATALT", DbType.DateTime, DBNull.Value);
            objDB.AddInParameter(DataBaseCommand, "FLGENV", DbType.String, "S");

            objDB.ExecuteNonQuery(DataBaseCommand, transaction);
        }

        public void UpdateItem(Product product, Price price, string scope)
        {
            string sqlCmd = @"UPDATE MRT.CADPCOPRDCTLAPIMKP i
                                 SET 
                                 i.NUMIDTMERAPIMKP = :NUMIDTMERAPIMKP,
                                 i.NUMIDTEMPSRRAPIMKP = :NUMIDTEMPSRRAPIMKP,
                                 i.VLRVNDMER = :VLRVNDMER,
                                 i.DATALT = :DATALT,
                                 i.FLGENV = :FLGENV
                                 WHERE i.NUMIDTMERAPIMKP = :NUMIDTMERAPIMKP 
                                 AND i.NUMIDTEMPSRRAPIMKP = :NUMIDTEMPSRRAPIMKP
                                 AND i.DESLSTPCOENVFRN = :DESLSTPCOENVFRN";


            DbCommand DataBaseCommand = objDB.GetSqlStringCommand(sqlCmd.ToString());

            objDB.AddInParameter(DataBaseCommand, "NUMIDTMERAPIMKP", DbType.Int64, product.product_id);
            objDB.AddInParameter(DataBaseCommand, "NUMIDTEMPSRRAPIMKP", DbType.Int64, product.seller_code);
            objDB.AddInParameter(DataBaseCommand, "VLRVNDMER", DbType.Double, price.sale_price);
            objDB.AddInParameter(DataBaseCommand, "DATALT", DbType.DateTime, DateTime.Now);
            objDB.AddInParameter(DataBaseCommand, "FLGENV", DbType.String, "S");
            objDB.AddInParameter(DataBaseCommand, "DESLSTPCOENVFRN", DbType.String, scope.Trim());

            int qtd = objDB.ExecuteNonQuery(DataBaseCommand, transaction);

            if (qtd == 0)
            {
                this.InsertItem(product, price, scope);
            }
        }

        /// <summary>
        /// Consulta dados do produto na interface;
        /// </summary>
        public string GetDadosProduto(long product_id, long seller_code, string erp_code)
        {
            string sku = string.Empty;
            string sqlCmd = string.Format(
                @"select p.CODMERSRR 
                  from MRT.CADPRDCTLOBTAPIMKP p 
                  where p.NUMIDTMERAPIMKP = :NUMIDTMERAPIMKP
                    and p.CODMERERPSRR = :CODMERERPSRR
                    and p.NUMIDTEMPSRRAPIMKP = :NUMIDTEMPSRRAPIMKP").Replace("\n", "").Replace("\t", "").Replace("\r", "");

            DbCommand DataBaseCommand = objDB.GetSqlStringCommand(sqlCmd);

            objDB.AddInParameter(DataBaseCommand, "CODMERERPSRR", DbType.String, erp_code.Trim());
            objDB.AddInParameter(DataBaseCommand, "NUMIDTMERAPIMKP", DbType.Int64, product_id);
            objDB.AddInParameter(DataBaseCommand, "NUMIDTEMPSRRAPIMKP", DbType.Int64, seller_code);

            IDataReader idr = objDB.ExecuteReader(DataBaseCommand);

            if (idr.Read())
                sku = idr["CODMERSRR"].ToString();

            if (idr != null)
            {
                idr.Close();
                idr.Dispose();
            }

            return sku.Trim();
        }

        public List<long> GetSellersExceto_MrtDemoHayamax()
        {
            List<long> sellers = new List<long>();

            StringBuilder SqlCmd = new StringBuilder();

            SqlCmd.AppendLine("SELECT T.NUMIDTEMPSRRAPIMKP");
            SqlCmd.AppendLine("FROM MRT.CADEMPSRR T");
            SqlCmd.AppendLine("WHERE T.CODEMPSRR NOT IN(1, 2)");
            SqlCmd.AppendLine("  AND T.NUMIDTEMPSRRAPIMKP IS NOT NULL");
            SqlCmd.AppendLine("ORDER BY T.CODEMPSRR");

            DbCommand DataBaseCommand = objDB.GetSqlStringCommand(SqlCmd.ToString());

            IDataReader idr = objDB.ExecuteReader(DataBaseCommand);

            while (idr.Read())
                sellers.Add(Convert.ToInt64(idr["NUMIDTEMPSRRAPIMKP"]));

            if (idr != null)
            {
                idr.Close();
                idr.Dispose();
            }

            return sellers;
        }
    }
}
