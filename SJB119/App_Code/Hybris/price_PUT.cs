﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar o objeto de request do método PutPrice;
    /// </summary>
    [XmlRoot("pricerow")]
    public class price_PUT
    {
        [XmlAttribute]
        public string pk { get; set; }

        [XmlAttribute]
        public string uri { get; set; }

        public ug ug { get; set; }

        public double price { get; set; }
    }
}
