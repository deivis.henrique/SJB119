﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar o response do método GetUserPriceGroup;
    /// </summary>
    [XmlRoot("enumerationvalue")]
    public class userPriceGroup
    {
        [XmlAttribute]
        public string code { get; set; }

        [XmlAttribute]
        public string pk { get; set; }

        [XmlAttribute]
        public string uri { get; set; }

        public string comments { get; set; }

        public DateTime creationtime { get; set; }

        public DateTime modifiedtime { get; set; }

        public string codeLowerCase { get; set; }

        public bool deprecated { get; set; }

        public int sequenceNumber { get; set; }
    }
}
