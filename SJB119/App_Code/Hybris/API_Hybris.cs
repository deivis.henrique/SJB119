﻿using SJB119.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding => Encoding.UTF8;
    }

    public class API_Hybris
    {
        /// <summary>
        /// Método utilizado para atualizar os dados do preço;
        /// </summary>
        /// <param name="sku">sku</param>
        /// <param name="objectRequest">Objeto com dados de request</param>
        /// <param name="objRequestSerialize">ref - Objeto com dados de request serializados</param>
        /// <param name="objResponseSerialize">ref - Objeto com dados de response serializados</param>
        /// <param name="msgFalhaComunicacaoAPI">ref - Mensagem caso ocorra falha de comunicação com API</param>
        /// <param name="httpStatusCode">ref - httpStatusCode</param>
        /// <returns>Retorna objeto com dados de response</returns>
        public PutPriceResponse PutPrice(string sku, price_PUT objectRequest, ref string objRequestSerialize, ref string objResponseSerialize, ref string msgFalhaComunicacaoAPI, ref HttpStatusCode httpStatusCode, string arg)
        {
            PutPriceResponse objPutPriceResponse = null;
            HttpWebResponse response = null;
            Stream dataStream = null;
            StreamReader sReader = null;
            string uri = string.Empty;

            try
            {
                Log.GravaLog("       # PutPrice {" + objectRequest.ug.code + "} ::: >>>SKU: " + sku.Trim() + " >>>PREÇO: " + objectRequest.price.ToString(), false, false, arg);

                productData productData = this.GetDataProduct(sku, null, ref objRequestSerialize, ref objResponseSerialize, ref msgFalhaComunicacaoAPI, ref httpStatusCode, ref uri, arg);

                if ((httpStatusCode == HttpStatusCode.OK) && (productData != null)
                    && (productData.catalogVersion != null) && (productData.unit != null))
                {
                    // Limpa variáveis;
                    httpStatusCode = HttpStatusCode.NotImplemented;
                    objRequestSerialize = string.Empty;
                    objResponseSerialize = string.Empty;
                    msgFalhaComunicacaoAPI = string.Empty;

                    productPrice productPrice = this.GetPriceProduct(sku, objectRequest.ug.code, null, ref objRequestSerialize, ref objResponseSerialize, ref msgFalhaComunicacaoAPI, ref httpStatusCode, arg);

                    if ((httpStatusCode == HttpStatusCode.OK) && (productPrice != null)
                        && (productPrice.ownEurope1Prices != null) && (productPrice.ownEurope1Prices.priceRow != null)
                        && (productPrice.ownEurope1Prices.priceRow.Count > 0))
                    {
                        // ********************** //
                        // *** ATUALIZA PRECO *** //
                        // ********************** //
                        priceRow priceRow = productPrice.ownEurope1Prices.priceRow.First();

                        // Limpa variáveis;
                        httpStatusCode = HttpStatusCode.NotImplemented;
                        objRequestSerialize = string.Empty;
                        objResponseSerialize = string.Empty;
                        msgFalhaComunicacaoAPI = string.Empty;

                        string authorization = string.Empty;
                        string urlPrincipalApi = string.Empty;
                        uri = string.Empty;

                        // Obtém Parâmetros de Comunicação com API;
                        this.GetParametersAPI(ref authorization, ref uri, string.Empty, ref urlPrincipalApi);

                        // Atualiza URI;
                        uri = priceRow.uri.Trim();

                        // Atualiza Informações de request;
                        objectRequest.uri = uri.Trim();
                        objectRequest.pk = priceRow.pk.Trim(); //uri.Trim().Split('/')[(uri.Trim().Split('/').Length - 1)].Trim();

                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(uri));

                        ////request.Timeout = 180000 '3 Minutos = 180000 Milissegundos
                        request.Credentials = CredentialCache.DefaultCredentials;
                        request.Method = "PUT";
                        request.ContentType = "application/xml;charset=UTF-8";

                        request.Headers.Add("Authorization", authorization.Trim());

                        if (objectRequest != null)
                        {
                            //////objPutOrderSellerRequestSerialize = new JavaScriptSerializer().Serialize(objectRequest);
                            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                            XmlSerializer serReq = new XmlSerializer(typeof(price_PUT));
                            ns.Add(string.Empty, string.Empty);
                            using (var sww = new Utf8StringWriter())
                            {
                                using (XmlWriter writer = XmlWriter.Create(sww))
                                {
                                    writer.WriteStartDocument(true); // that bool parameter is called "standalone";
                                    serReq.Serialize(writer, objectRequest, ns);
                                    objRequestSerialize = sww.ToString(); // XML
                                }
                            }

                            byte[] byteArray = Encoding.UTF8.GetBytes(objRequestSerialize);

                            request.ContentLength = byteArray.Length;

                            dataStream = request.GetRequestStream();

                            dataStream.Write(byteArray, 0, byteArray.Length);
                            dataStream.Close();
                        }
                        else
                            request.ContentLength = 0;

                        response = (HttpWebResponse)request.GetResponse();

                        httpStatusCode = response.StatusCode;
                        sReader = new StreamReader(response.GetResponseStream());

                        string strJson = sReader.ReadToEnd();

                        if (httpStatusCode != HttpStatusCode.OK)
                            throw new WebException(strJson);

                        //////objPutOrderSellerResponse = new JavaScriptSerializer().Deserialize<PutOrderSellerResponse>(strJson);
                        if (!string.IsNullOrEmpty(strJson))
                        {
                            XmlSerializer serResp = new XmlSerializer(typeof(PutPriceResponse));
                            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(strJson));
                            objPutPriceResponse = (PutPriceResponse)serResp.Deserialize(memStream);
                        }

                        objResponseSerialize = new JavaScriptSerializer().Serialize(objPutPriceResponse);
                    }
                    else
                    {
                        Log.GravaLog("            -->[GetPriceProduct]       # Request: " + objRequestSerialize + "\n                  # Response: " + objResponseSerialize + " \n                  # Msg_Falha: " + msgFalhaComunicacaoAPI, false, false, arg);

                        // ****************** //
                        // *** CRIA PRECO *** //
                        // ****************** //
                        // Limpa variáveis;
                        httpStatusCode = HttpStatusCode.NotImplemented;
                        objRequestSerialize = string.Empty;
                        objResponseSerialize = string.Empty;
                        msgFalhaComunicacaoAPI = string.Empty;

                        string authorization = string.Empty;
                        string urlPrincipalApi = string.Empty;
                        uri = string.Empty;

                        // Obtém Parâmetros de Comunicação com API;
                        this.GetParametersAPI(ref authorization, ref uri, "/pricerows", ref urlPrincipalApi);

                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(uri));

                        ////request.Timeout = 180000 '3 Minutos = 180000 Milissegundos
                        request.Credentials = CredentialCache.DefaultCredentials;
                        request.Method = "POST";
                        request.ContentType = "application/xml;charset=UTF-8";

                        request.Headers.Add("Authorization", authorization.Trim());

                        // Popula objeto de request;
                        price_POST objPrice_POST = new price_POST
                        {
                            product = new productDataPricePOST
                            {
                                code = productData.code,
                                pk = productData.pk,
                                uri = productData.uri,
                                catalogVersion = productData.catalogVersion
                            },
                            currency = new currency
                            {
                                isocode = "BRL" //,
                                                //pk = pk,
                                                //uri = urlPrincipalApi + "/currencies/BRL"
                            },
                            price = objectRequest.price,
                            unit = productData.unit,
                            ug = objectRequest.ug
                        };

                        if (objPrice_POST != null)
                        {
                            //////objPutOrderSellerRequestSerialize = new JavaScriptSerializer().Serialize(objectRequest);
                            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                            XmlSerializer serReq = new XmlSerializer(typeof(price_POST));
                            ns.Add(string.Empty, string.Empty);
                            using (var sww = new Utf8StringWriter())
                            {
                                using (XmlWriter writer = XmlWriter.Create(sww))
                                {
                                    writer.WriteStartDocument(true); // that bool parameter is called "standalone";
                                    serReq.Serialize(writer, objPrice_POST, ns);
                                    objRequestSerialize = sww.ToString(); // XML
                                }
                            }

                            byte[] byteArray = Encoding.UTF8.GetBytes(objRequestSerialize);

                            request.ContentLength = byteArray.Length;

                            dataStream = request.GetRequestStream();

                            dataStream.Write(byteArray, 0, byteArray.Length);
                            dataStream.Close();
                        }
                        else
                            request.ContentLength = 0;

                        response = (HttpWebResponse)request.GetResponse();

                        httpStatusCode = response.StatusCode;
                        sReader = new StreamReader(response.GetResponseStream());

                        string strJson = sReader.ReadToEnd();

                        if (httpStatusCode != HttpStatusCode.Created)
                            throw new WebException(strJson);

                        //////objPutOrderSellerResponse = new JavaScriptSerializer().Deserialize<PutOrderSellerResponse>(strJson);
                        PostPriceResponse objPostPriceResponse = null;
                        if (!string.IsNullOrEmpty(strJson))
                        {
                            XmlSerializer serResp = new XmlSerializer(typeof(PostPriceResponse));
                            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(strJson));
                            objPostPriceResponse = (PostPriceResponse)serResp.Deserialize(memStream);
                        }

                        objResponseSerialize = new JavaScriptSerializer().Serialize(objPostPriceResponse);
                    }
                }
                else
                {
                    httpStatusCode = HttpStatusCode.NotImplemented;
                    Log.GravaLog("            -->[GetDataProduct]\n                  # URI: " + uri + " \n                  # Request: " + objRequestSerialize + "\n                  # Response: " + objResponseSerialize + " \n                  # Msg_Falha: " + msgFalhaComunicacaoAPI, false, false, arg);
                }
            }
            catch (WebException ex)
            {
                ////Throw ex
                msgFalhaComunicacaoAPI = ex.Message.Trim();

                string erroComunicacaoAPI = " # Dados Request: " + objRequestSerialize;

                erroComunicacaoAPI += " ### ERRO: " + ex.ToString();
                string msgResponse = string.Empty;
                if (ex.Response != null)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)ex.Response;
                    Stream dataResponse = ex.Response.GetResponseStream();
                    StreamReader readerResponse = new StreamReader(dataResponse);
                    httpStatusCode = httpResponse.StatusCode;
                    msgResponse = readerResponse.ReadToEnd();
                    erroComunicacaoAPI += " ### RESPONSE: " + httpResponse.StatusCode + " - " + msgResponse;
                }

                //////Email.EnviaEmail(ConfigurationManager.AppSettings.GetValues("DestinatarioTI").First(), erroComunicacaoAPI, "SJB119: Erro PutPrice", false);

                //Tratativa para exibir mensagem de erro amigável;
                //////if ((msgResponse.Trim().Length > 0) && (msgResponse.Contains("\"Message\":")))
                //////{
                //////    msgFalhaComunicacaoAPI = string.Empty;
                //////    int startIndex = 0;
                //////    int length = 0;
                //////    while (msgResponse.Contains("\"Message\":"))
                //////    {
                //////        startIndex = msgResponse.IndexOf("\"Message\":");
                //////        msgResponse = msgResponse.Substring(startIndex, (msgResponse.Length - 1) - startIndex);
                //////        length = msgResponse.IndexOf("}");
                //////        msgFalhaComunicacaoAPI += msgResponse.Substring(0, length).Replace("\"Message\":", string.Empty).Trim() + ";";
                //////        msgResponse = msgResponse.Replace(msgResponse.Substring(0, length), string.Empty);
                //////    }
                //////}
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response.Dispose();
                }

                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream.Dispose();
                }

                if (sReader != null)
                {
                    sReader.Close();
                    sReader.Dispose();
                }
            }

            return objPutPriceResponse;
        }

        /// <summary>
        /// Método utilizado para obter os dados do preço;
        /// </summary>
        /// <param name="sku">sku</param>
        /// <param name="ugCode">ugCode</param>
        /// <param name="objectRequest">Objeto com dados de request</param>
        /// <param name="objGetPriceProductRequestSerialize">ref - Objeto com dados de request serializados</param>
        /// <param name="objGetPriceProductResponseSerialize">ref - Objeto com dados de response serializados</param>
        /// <param name="msgFalhaComunicacaoAPI">ref - Mensagem caso ocorra falha de comunicação com API</param>
        /// <param name="httpStatusCode">ref - httpStatusCode</param>
        /// <returns>Retorna objeto com dados de response</returns>
        public productPrice GetPriceProduct(string sku, string ugCode, Object objectRequest, ref string objGetPriceProductRequestSerialize, ref string objGetPriceProductResponseSerialize, ref string msgFalhaComunicacaoAPI, ref HttpStatusCode httpStatusCode, string arg)
        {
            productPrice objGetPriceProductResponse = null;

            userPriceGroup uPriceGroup = GetUserPriceGroup(ugCode, objectRequest, ref objGetPriceProductRequestSerialize, ref objGetPriceProductResponseSerialize, ref msgFalhaComunicacaoAPI, ref httpStatusCode, arg);

            if ((uPriceGroup != null) 
                && (!string.IsNullOrEmpty(uPriceGroup.pk)) 
                && (httpStatusCode == HttpStatusCode.OK))
            {
                // Limpa variáveis;
                httpStatusCode = HttpStatusCode.NotImplemented;
                objGetPriceProductRequestSerialize = string.Empty;
                msgFalhaComunicacaoAPI = string.Empty;

                HttpWebResponse response = null;
                Stream dataStream = null;
                StreamReader sReader = null;

                try
                {
                    string authorization = string.Empty;
                    string uri = string.Empty;
                    string urlPrincipalApi = string.Empty;

                    // Obtém Parâmetros de Comunicação com API;
                    this.GetParametersAPI(ref authorization, ref uri, String.Format("/catalogs/{0}ProductCatalog/catalogversions/Staged/products/{1}?ownEurope1Prices_query=%7BuserMatchQualifier%7D={2}&product_attributes=code,ownEurope1Prices&pricerow_attributes=price,userMatchQualifier,pk", sku.Trim().Split('_')[0].ToLower().Trim(), sku.Trim(), uPriceGroup.pk.Trim()), ref urlPrincipalApi);

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(uri));

                    ////request.Timeout = 180000 '3 Minutos = 180000 Milissegundos
                    request.Credentials = CredentialCache.DefaultCredentials;
                    request.Method = "GET";
                    request.ContentType = "application/xml;charset=UTF-8";

                    request.Headers.Add("Authorization", authorization.Trim());

                    if (objectRequest != null)
                    {
                        objGetPriceProductRequestSerialize = new JavaScriptSerializer().Serialize(objectRequest);

                        byte[] byteArray = Encoding.UTF8.GetBytes(objGetPriceProductRequestSerialize);

                        request.ContentLength = byteArray.Length;

                        dataStream = request.GetRequestStream();

                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                    }
                    else
                        request.ContentLength = 0;

                    response = (HttpWebResponse)request.GetResponse();

                    httpStatusCode = response.StatusCode;
                    sReader = new StreamReader(response.GetResponseStream());

                    string strJson = sReader.ReadToEnd();

                    if (httpStatusCode != HttpStatusCode.OK)
                        throw new WebException(strJson);

                    // Tratativa p/ trocar "elementName" do XmlRoot caso necessário;
                    XDocument doc = XDocument.Parse(strJson);
                    if (doc.Root.Name != "product")
                        doc.Root.Name = "product";

                    //////objGetOrderSellerResponse = new JavaScriptSerializer().Deserialize<consignments>(strJson);
                    XmlSerializer ser = new XmlSerializer(typeof(productPrice));
                    MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(doc.ToString()));
                    objGetPriceProductResponse = (productPrice)ser.Deserialize(memStream);

                    objGetPriceProductResponseSerialize = new JavaScriptSerializer().Serialize(objGetPriceProductResponse);
                }
                catch (WebException ex)
                {
                    ////Throw ex
                    msgFalhaComunicacaoAPI = ex.Message.Trim();

                    string erroComunicacaoAPI = " # Dados Request: " + objGetPriceProductRequestSerialize;

                    erroComunicacaoAPI += " ### ERRO: " + ex.ToString();
                    string msgResponse = string.Empty;
                    HttpWebResponse httpResponse = null;
                    if (ex.Response != null)
                    {
                        httpResponse = (HttpWebResponse)ex.Response;
                        Stream dataResponse = ex.Response.GetResponseStream();
                        StreamReader readerResponse = new StreamReader(dataResponse);
                        httpStatusCode = httpResponse.StatusCode;
                        msgResponse = readerResponse.ReadToEnd();
                        erroComunicacaoAPI += " ### RESPONSE: " + httpResponse.StatusCode + " - " + msgResponse;
                    }

                    if ((httpResponse == null) || (httpResponse.StatusCode != HttpStatusCode.NotFound))
                        Email.EnviaEmail(ConfigurationManager.AppSettings.GetValues("DestinatarioTI").First(), erroComunicacaoAPI, "SJB119: Erro GetPriceProduct", false);
                    else
                    {
                        Log.GravaLog("       # GetPriceProduct[" + httpResponse.StatusCode + "] - SKU: " + sku.Trim(), false, false, arg);
                    }

                    //Tratativa para exibir mensagem de erro amigável;
                    //////if ((msgResponse.Trim().Length > 0) && (msgResponse.Contains("\"Message\":")))
                    //////{
                    //////    msgFalhaComunicacaoAPI = string.Empty;
                    //////    int startIndex = 0;
                    //////    int length = 0;
                    //////    while (msgResponse.Contains("\"Message\":"))
                    //////    {
                    //////        startIndex = msgResponse.IndexOf("\"Message\":");
                    //////        msgResponse = msgResponse.Substring(startIndex, (msgResponse.Length - 1) - startIndex);
                    //////        length = msgResponse.IndexOf("}");
                    //////        msgFalhaComunicacaoAPI += msgResponse.Substring(0, length).Replace("\"Message\":", string.Empty).Trim() + ";";
                    //////        msgResponse = msgResponse.Replace(msgResponse.Substring(0, length), string.Empty);
                    //////    }
                    //////}
                }
                finally
                {
                    if (response != null)
                    {
                        response.Close();
                        response.Dispose();
                    }

                    if (dataStream != null)
                    {
                        dataStream.Close();
                        dataStream.Dispose();
                    }

                    if (sReader != null)
                    {
                        sReader.Close();
                        sReader.Dispose();
                    }
                }
            }

            return objGetPriceProductResponse;
        }

        /// <summary>
        /// Método utilizado para obter os dados da lista de preço;
        /// </summary>
        /// <param name="ugCode">ugCode</param>
        /// <param name="objectRequest">Objeto com dados de request</param>
        /// <param name="objGetUserPriceGroupRequestSerialize">ref - Objeto com dados de request serializados</param>
        /// <param name="objGetUserPriceGroupResponseSerialize">ref - Objeto com dados de response serializados</param>
        /// <param name="msgFalhaComunicacaoAPI">ref - Mensagem caso ocorra falha de comunicação com API</param>
        /// <param name="httpStatusCode">ref - httpStatusCode</param>
        /// <returns>Retorna objeto com dados de response</returns>
        public userPriceGroup GetUserPriceGroup(string ugCode, Object objectRequest, ref string objGetUserPriceGroupRequestSerialize, ref string objGetUserPriceGroupResponseSerialize, ref string msgFalhaComunicacaoAPI, ref HttpStatusCode httpStatusCode, string arg)
        {
            userPriceGroup objGetUserPriceGroupResponse = null;
            HttpWebResponse response = null;
            Stream dataStream = null;
            StreamReader sReader = null;

            try
            {
                string authorization = string.Empty;
                string uri = string.Empty;
                string urlPrincipalApi = string.Empty;

                // Obtém Parâmetros de Comunicação com API;
                this.GetParametersAPI(ref authorization, ref uri, String.Format("/enumerationmetatypes/UserPriceGroup/{0}", ugCode.Trim()), ref urlPrincipalApi);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(uri));

                ////request.Timeout = 180000 '3 Minutos = 180000 Milissegundos
                request.Credentials = CredentialCache.DefaultCredentials;
                request.Method = "GET";
                request.ContentType = "application/xml;charset=UTF-8";

                request.Headers.Add("Authorization", authorization.Trim());

                if (objectRequest != null)
                {
                    objGetUserPriceGroupRequestSerialize = new JavaScriptSerializer().Serialize(objectRequest);

                    byte[] byteArray = Encoding.UTF8.GetBytes(objGetUserPriceGroupRequestSerialize);

                    request.ContentLength = byteArray.Length;

                    dataStream = request.GetRequestStream();

                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }
                else
                    request.ContentLength = 0;

                response = (HttpWebResponse)request.GetResponse();

                httpStatusCode = response.StatusCode;
                sReader = new StreamReader(response.GetResponseStream());

                string strJson = sReader.ReadToEnd();

                if (httpStatusCode != HttpStatusCode.OK)
                    throw new WebException(strJson);

                //////objGetOrderSellerResponse = new JavaScriptSerializer().Deserialize<consignments>(strJson);
                XmlSerializer ser = new XmlSerializer(typeof(userPriceGroup));
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(strJson));
                objGetUserPriceGroupResponse = (userPriceGroup)ser.Deserialize(memStream);

                objGetUserPriceGroupResponseSerialize = new JavaScriptSerializer().Serialize(objGetUserPriceGroupResponse);
            }
            catch (WebException ex)
            {
                ////Throw ex
                msgFalhaComunicacaoAPI = ex.Message.Trim();

                string erroComunicacaoAPI = " # Dados Request: " + objGetUserPriceGroupRequestSerialize;

                erroComunicacaoAPI += " ### ERRO: " + ex.ToString();
                string msgResponse = string.Empty;
                HttpWebResponse httpResponse = null;
                if (ex.Response != null)
                {
                    httpResponse = (HttpWebResponse)ex.Response;
                    Stream dataResponse = ex.Response.GetResponseStream();
                    StreamReader readerResponse = new StreamReader(dataResponse);
                    httpStatusCode = httpResponse.StatusCode;
                    msgResponse = readerResponse.ReadToEnd();
                    erroComunicacaoAPI += " ### RESPONSE: " + httpResponse.StatusCode + " - " + msgResponse;
                }

                if ((httpResponse == null) || (httpResponse.StatusCode != HttpStatusCode.NotFound))
                    Email.EnviaEmail(ConfigurationManager.AppSettings.GetValues("DestinatarioTI").First(), erroComunicacaoAPI, "SJB119: Erro GetUserPriceGroup", false);
                else
                {
                    Log.GravaLog("       # GetUserPriceGroup[" + httpResponse.StatusCode + "] - LISTA: " + ugCode.Trim(), false, false, arg);
                }

                //Tratativa para exibir mensagem de erro amigável;
                //////if ((msgResponse.Trim().Length > 0) && (msgResponse.Contains("\"Message\":")))
                //////{
                //////    msgFalhaComunicacaoAPI = string.Empty;
                //////    int startIndex = 0;
                //////    int length = 0;
                //////    while (msgResponse.Contains("\"Message\":"))
                //////    {
                //////        startIndex = msgResponse.IndexOf("\"Message\":");
                //////        msgResponse = msgResponse.Substring(startIndex, (msgResponse.Length - 1) - startIndex);
                //////        length = msgResponse.IndexOf("}");
                //////        msgFalhaComunicacaoAPI += msgResponse.Substring(0, length).Replace("\"Message\":", string.Empty).Trim() + ";";
                //////        msgResponse = msgResponse.Replace(msgResponse.Substring(0, length), string.Empty);
                //////    }
                //////}
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response.Dispose();
                }

                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream.Dispose();
                }

                if (sReader != null)
                {
                    sReader.Close();
                    sReader.Dispose();
                }
            }

            return objGetUserPriceGroupResponse;
        }

        /// <summary>
        /// Método utilizado para obter os dados do produto;
        /// </summary>
        /// <param name="sku">sku</param>
        /// <param name="objectRequest">Objeto com dados de request</param>
        /// <param name="objGetDataProductRequestSerialize">ref - Objeto com dados de request serializados</param>
        /// <param name="objGetDataProductResponseSerialize">ref - Objeto com dados de response serializados</param>
        /// <param name="msgFalhaComunicacaoAPI">ref - Mensagem caso ocorra falha de comunicação com API</param>
        /// <param name="httpStatusCode">ref - httpStatusCode</param>
        /// <returns>Retorna objeto com dados de response</returns>
        public productData GetDataProduct(string sku, Object objectRequest, ref string objGetDataProductRequestSerialize, ref string objGetDataProductResponseSerialize, ref string msgFalhaComunicacaoAPI, ref HttpStatusCode httpStatusCode, ref string uri, string arg)
        {
            productData objGetDataProductResponse = null;
            HttpWebResponse response = null;
            Stream dataStream = null;
            StreamReader sReader = null;

            uri = string.Empty;
            try
            {
                string authorization = string.Empty;
                string urlPrincipalApi = string.Empty;
                string catalogName = sku.Trim().Split('_')[0].ToLower().Trim() + "ProductCatalog";

                // Obtém Parâmetros de Comunicação com API;
                this.GetParametersAPI(ref authorization, ref uri, String.Format("/catalogs/{0}/catalogversions/Staged/products/{1}", catalogName, sku.Trim()), ref urlPrincipalApi);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(uri));

                ////request.Timeout = 180000 '3 Minutos = 180000 Milissegundos
                request.Credentials = CredentialCache.DefaultCredentials;
                request.Method = "GET";
                request.ContentType = "application/xml;charset=UTF-8";

                request.Headers.Add("Authorization", authorization.Trim());

                if (objectRequest != null)
                {
                    objGetDataProductRequestSerialize = new JavaScriptSerializer().Serialize(objectRequest);

                    byte[] byteArray = Encoding.UTF8.GetBytes(objGetDataProductRequestSerialize);

                    request.ContentLength = byteArray.Length;

                    dataStream = request.GetRequestStream();

                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }
                else
                    request.ContentLength = 0;

                response = (HttpWebResponse)request.GetResponse();

                httpStatusCode = response.StatusCode;
                sReader = new StreamReader(response.GetResponseStream());

                string strJson = sReader.ReadToEnd();

                if (httpStatusCode != HttpStatusCode.OK)
                    throw new WebException(strJson);

                // Tratativa p/ trocar "elementName" do XmlRoot caso necessário;
                XDocument doc = XDocument.Parse(strJson);
                if (doc.Root.Name != "product")
                    doc.Root.Name = "product";

                //////objGetOrderSellerResponse = new JavaScriptSerializer().Deserialize<consignments>(strJson);
                XmlSerializer ser = new XmlSerializer(typeof(productData));
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(doc.ToString()));
                objGetDataProductResponse = (productData)ser.Deserialize(memStream);

                // Atualiza "catalogName" no objeto de response;
                objGetDataProductResponse.catalogVersion.catalog = new catalog { id = catalogName };

                objGetDataProductResponseSerialize = new JavaScriptSerializer().Serialize(objGetDataProductResponse);
            }
            catch (WebException ex)
            {
                ////Throw ex
                msgFalhaComunicacaoAPI = ex.Message.Trim();

                string erroComunicacaoAPI = " # Dados Request: " + objGetDataProductRequestSerialize;

                erroComunicacaoAPI += " ### ERRO: " + ex.ToString();
                string msgResponse = string.Empty;
                HttpWebResponse httpResponse = null;
                if (ex.Response != null)
                {
                    httpResponse = (HttpWebResponse)ex.Response;
                    Stream dataResponse = ex.Response.GetResponseStream();
                    StreamReader readerResponse = new StreamReader(dataResponse);
                    httpStatusCode = httpResponse.StatusCode;
                    msgResponse = readerResponse.ReadToEnd();
                    erroComunicacaoAPI += " ### RESPONSE: " + httpResponse.StatusCode + " - " + msgResponse;
                }

                if ((httpResponse == null) || (httpResponse.StatusCode != HttpStatusCode.NotFound))
                    Email.EnviaEmail(ConfigurationManager.AppSettings.GetValues("DestinatarioTI").First(), erroComunicacaoAPI, "SJB119: Erro GetDataProduct", false);
                else
                {
                    Log.GravaLog("       # GetDataProduct[" + httpResponse.StatusCode + "] - SKU: " + sku.Trim() + ", URI: " + uri, false, false, arg);
                }

                //Tratativa para exibir mensagem de erro amigável;
                //////if ((msgResponse.Trim().Length > 0) && (msgResponse.Contains("\"Message\":")))
                //////{
                //////    msgFalhaComunicacaoAPI = string.Empty;
                //////    int startIndex = 0;
                //////    int length = 0;
                //////    while (msgResponse.Contains("\"Message\":"))
                //////    {
                //////        startIndex = msgResponse.IndexOf("\"Message\":");
                //////        msgResponse = msgResponse.Substring(startIndex, (msgResponse.Length - 1) - startIndex);
                //////        length = msgResponse.IndexOf("}");
                //////        msgFalhaComunicacaoAPI += msgResponse.Substring(0, length).Replace("\"Message\":", string.Empty).Trim() + ";";
                //////        msgResponse = msgResponse.Replace(msgResponse.Substring(0, length), string.Empty);
                //////    }
                //////}
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response.Dispose();
                }

                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream.Dispose();
                }

                if (sReader != null)
                {
                    sReader.Close();
                    sReader.Dispose();
                }
            }

            return objGetDataProductResponse;
        }

        /// <summary>
        /// Obtém Parâmetros de Comunicação com API;
        /// </summary>
        /// <param name="authorization">ref - authorization</param>
        /// <param name="uri">ref - uri</param>
        /// <param name="complementoUri">complementoUri</param>
        /// <param name="urlPrincipalApi">urlApi</param>
        private void GetParametersAPI(ref string authorization, ref string uri, string complementoUri, ref string urlPrincipalApi)
        {
            ExeConfigurationFileMap configExternoMap = new ExeConfigurationFileMap
            {
                ExeConfigFilename = ConfigurationManager.AppSettings.GetValues("Path_MPlace").First()
            };

            Configuration configExternoFile = ConfigurationManager.OpenMappedExeConfiguration(configExternoMap, ConfigurationUserLevel.None);

            // Obtém Parâmetros de Produção da Braspag;
            urlPrincipalApi = configExternoFile.AppSettings.Settings["UrlApiHybris_PRODUCAO"].Value.Trim();
            string credentials = String.Format("{0}:{1}", configExternoFile.AppSettings.Settings["UserNameApiHybris_PRODUCAO"].Value.Trim(), configExternoFile.AppSettings.Settings["PasswordApiHybris_PRODUCAO"].Value.Trim());
            byte[] bytes = Encoding.ASCII.GetBytes(credentials);
            string base64 = Convert.ToBase64String(bytes);
            authorization = String.Concat("Basic ", base64);

            // "Tombo" para Utilizar Parâmetros de Homologação;
            if (Dns.GetHostName().Trim().ToUpper() != "BETRIA")
            {
                urlPrincipalApi = configExternoFile.AppSettings.Settings["UrlApiHybris_HOMOLOGACAO"].Value.Trim();
                credentials = String.Format("{0}:{1}", configExternoFile.AppSettings.Settings["UserNameApiHybris_HOMOLOGACAO"].Value.Trim(), configExternoFile.AppSettings.Settings["PasswordApiHybris_HOMOLOGACAO"].Value.Trim());
                bytes = Encoding.ASCII.GetBytes(credentials);
                base64 = Convert.ToBase64String(bytes);
                authorization = String.Concat("Basic ", base64);
            }

            uri = urlPrincipalApi + complementoUri;
        }
    }
}
