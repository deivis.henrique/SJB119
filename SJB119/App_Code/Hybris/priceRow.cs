﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar o objeto priceRow;
    /// </summary>
    public class priceRow
    {
        [XmlAttribute]
        public string pk { get; set; }

        [XmlAttribute]
        public string uri { get; set; }

        public string userMatchQualifier { get; set; }
        public double price { get; set; }
    }
}
