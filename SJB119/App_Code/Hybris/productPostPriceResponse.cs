﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar "productPostPriceResponse"
    /// </summary>
    public class productPostPriceResponse
    {
        [XmlAttribute]
        public string code { get; set; }

        [XmlAttribute]
        public string pk { get; set; }

        [XmlAttribute]
        public string uri { get; set; }

        public bool soldIndividually { get; set; }
    }
}
