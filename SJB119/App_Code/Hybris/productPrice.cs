﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar o response do método GetPrice;
    /// </summary>
    [XmlRoot("product")]
    public class productPrice
    {
        [XmlAttribute]
        public string code { get; set; }

        [XmlAttribute]
        public string uri { get; set; }

        [XmlElement("ownEurope1Prices")]
        public ownEurope1Prices ownEurope1Prices { get; set; }

        public bool soldIndividually { get; set; }
    }
}
