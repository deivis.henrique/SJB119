﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar os dados de "catalog";
    /// </summary>
    public class catalog
    {
        [XmlAttribute]
        public string id { get; set; }
    }
}
