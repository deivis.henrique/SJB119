﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar os dados de "catalogVersion";
    /// </summary>
    public class catalogVersion
    {
        [XmlAttribute]
        public string version { get; set; }

        [XmlAttribute]
        public string pk { get; set; }

        [XmlAttribute]
        public string uri { get; set; }

        [XmlElement("catalog")]
        public catalog catalog { get; set; }
    }
}
