﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar objeto "ug"
    /// </summary>
    public class ug
    {
        [XmlAttribute]
        public string code { get; set; }
    }
}
