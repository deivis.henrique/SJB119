﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar o objeto product no request do método PricePOST;
    /// </summary>
    [XmlRoot("product")]
    public class productDataPricePOST
    {
        [XmlAttribute]
        public string code { get; set; }

        [XmlAttribute]
        public string pk { get; set; }

        [XmlAttribute]
        public string uri { get; set; }

        [XmlElement("catalogVersion")]
        public catalogVersion catalogVersion { get; set; }
    }
}
