﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar "priceRowRoot"
    /// </summary>
    [XmlRoot("pricerow")]
    public class price_POST
    {
        [XmlElement("product")]
        public productDataPricePOST product { get; set; }

        [XmlElement("currency")]
        public currency currency { get; set; }

        public double price { get; set; }

        [XmlElement("unit")]
        public unit unit { get; set; }

        public ug ug { get; set; }
    }
}
