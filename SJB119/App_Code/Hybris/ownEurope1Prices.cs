﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar o objeto ownEurope1Prices;
    /// </summary>
    public class ownEurope1Prices
    {
        [XmlElement("priceRow")]
        public List<priceRow> priceRow { get; set; }
    }
}
