﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SJB119.App_Code.Hybris
{
    /// <summary>
    /// Classe utilizada para serializar o response do método PostPrice;
    /// </summary>
    [XmlType(AnonymousType = true)]
    [XmlRoot("pricerow")]
    public class PostPriceResponse
    {
        [XmlAttribute]
        public string pk { get; set; }

        [XmlAttribute]
        public string uri { get; set; }

        public string comments { get; set; }

        public DateTime creationtime { get; set; }

        public DateTime modifiedtime { get; set; }

        [XmlElement(ElementName = "product", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public productPostPriceResponse product { get; set; }

        public string productMatchQualifier { get; set; }

        public string userMatchQualifier { get; set; }

        [XmlElement("catalogVersion")]
        public catalogVersionPostPriceResponse catalogVersion { get; set; }

        [XmlElement("currency")]
        public currency currency { get; set; }

        public bool giveAwayPrice { get; set; }

        public string matchValue { get; set; }

        public string minqtd { get; set; }

        public bool net { get; set; }

        public double price { get; set; }

        [XmlElement("unit")]
        public unit unit { get; set; }

        public string unitFactor { get; set; }
    }
}
