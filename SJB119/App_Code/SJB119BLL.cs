﻿using SJB119.App_Code.MkpB2B;
using SJB119.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using SJB119.App_Code.Hybris;

namespace SJB119.App_Code
{
    public class SJB119BLL : Martins.Layers.BLL
    {
        private SJB119DAL DAL;

        public SJB119BLL()
        {
            DAL = new SJB119DAL();
        }

        public void Dispose()
        {
            if ((DAL != null) && (DAL.connection_MRT001 != null))
            {
                DAL.connection_MRT001.Close();
                DAL.connection_MRT001.Dispose();
                DAL.connection_MRT001 = null;
            }
            if (DAL != null)
            {
                DAL = null;
            }
        }

        public void ValidarPrecoOpenKComMartins() {
            try {
                //Resgata Lista Preço Produtos;
                MplaceService svc = new MplaceService();
                int total = 0;
                int _offset = 0;
                int _limit = 500;
                Items itens = null;
                List<long> sellers = DAL.GetSellersExceto_MrtDemoHayamax();

                foreach (long seller_id in sellers) {
                    // Reseta variáveis;
                    total = 0;
                    _offset = 0;

                    while (_offset == 0 || _offset < total) {
                        Log.GravaLog("      *** Resgatando Lista Preço Produtos Openk...", false, false, "");

                        itens = svc.GetListaPrecoItens(_offset, _limit, seller_id);

                        _offset += _limit;

                        if (total == 0 && itens != null)
                            total = itens.total;

                        if (itens != null && itens.products != null && itens.products.Count > 0) {
                            ProcessarPrecosSeller(itens, _offset, total, seller_id);
                        } else
                            Log.GravaLog(string.Format("      *** Nenhum item encontrado na OpenK com o seller_id = {0}", seller_id), false, false, "");
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void ValidarPrecoOpenKComMartinsComParemetro(string args) {
            try {
                List<long> sellers = DAL.GetSellersExceto_MrtDemoHayamax();

                foreach (long seller_id in sellers) {
                    ProcessarPrecosSellerComPorcentagem(seller_id, args);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ProcessarPrecosSeller(Items items, int _offset, int _total, long seller_id) {
            try {
                Log.GravaLog(string.Format("      *** Buscando Produtos com o id do seller {0}", seller_id.ToString()), false, false, "");
                List<ProductMartins> productMartins = DAL.BuscarProdutosSellerBaseMartins(seller_id);

                Log.GravaLog(string.Format("      *** Inicialização da verificação entre os valores da OpenK e da tabela MRT.CADPCOPRDCTLAPIMKP"), false, false, "");
                foreach (var product in productMartins) {
                    Product item = items.products.Where(x => x.product_id == Convert.ToInt64(product.NUMIDTMERAPIMKP)).FirstOrDefault();
                    if (item != null) {
                        foreach (var preco in item.prices) {
                            if (preco.sale_price != Convert.ToDouble(product.VLRVNDMER)) {
                                Log.GravaLog(string.Format("      *** O produto com id {0} está com o valor de R$ {1} na OpenK e com o valor de R$ {2} no banco do Martins", item.product_id, preco.sale_price, product.VLRVNDMER), false, false, "");

                                Email.EnviaEmail(ConfigurationManager.AppSettings.GetValues("DestinatarioTI").First(),
                                                 string.Format("O produto com id {0} está com o valor de {1} na OpenK e com o valor de {2} no banco do Martins",
                                                                item.product_id, preco.sale_price, product.VLRVNDMER),
                                                 "SJB119: Valor diferente entre Openk e o banco do Martins", false);
                            } else {
                                Log.GravaLog(string.Format("      *** O produto com id {0} está com o valor de {1} na OpenK e com o valor de {2} no banco do Martins", item.product_id, preco.sale_price, product.VLRVNDMER), false, false, "");
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }
        }

        public void ProcessarPrecosSellerComPorcentagem(long seller_id, string args) {
            try {
                Log.GravaLog(string.Format("      *** Buscando Produtos na tabela MRT.CADPCOPRDCTLAPIMKP com o id do seller {0}", seller_id.ToString()), false, false, "");
                List<ProductMartins> productBaseMartins = DAL.BuscarProdutosSellerBaseMartins(seller_id);

                Log.GravaLog(string.Format("      *** Buscando Produtos na tabela MRT.HSTPCOPRDCTLAPIMKP com o id do seller {0}", seller_id.ToString()), false, false, "");
                List<ProductHistoricoMartins> productHistoricoMartins = DAL.BuscarProdutosSellerBaseHistorico(seller_id);

                Log.GravaLog(string.Format("      *** Inicialização da verificação entre os valores das tabelas MRT.CADPCOPRDCTLAPIMKP e MRT.HSTPCOPRDCTLAPIMKP"), false, false, args);
                foreach (var produtoBase in productBaseMartins) {

                    ProductHistoricoMartins produtoHistorico = productHistoricoMartins.Where(x => x.NUMIDTEMPSRRAPIMKP == produtoBase.NUMIDTMERAPIMKP).FirstOrDefault();

                    if (produtoHistorico != null) {

                        if(Convert.ToInt64(produtoBase.VLRVNDMER) != 0 && Convert.ToInt64(produtoHistorico.VLRVNDMER) != 0) {

                            var diferencaPorcentagem = ((Convert.ToInt64(produtoBase.VLRVNDMER) - Convert.ToInt64(produtoHistorico.VLRVNDMER)) / Convert.ToInt64(produtoBase.VLRVNDMER)) * 100;

                            if (diferencaPorcentagem < 0)
                                diferencaPorcentagem = diferencaPorcentagem * (-1);

                            if (diferencaPorcentagem > Convert.ToInt64(args)) {
                                Log.GravaLog(string.Format("      *** O produto com id {0} contém o valor da diferenca em porcentagem é superior que a informada", produtoBase.NUMIDTMERAPIMKP), false, false, "");

                                Email.EnviaEmail(ConfigurationManager.AppSettings.GetValues("DestinatarioTI").First(),
                                                 string.Format("O produto com a descrição: {0} e com o id {1} contém o valor R$ {2} na tabela MRT.CADPCOPRDCTLAPIMKP e o valor {3} na tabela MRT.HSTPCOPRDCTLAPIMKP, gerando uma diferença de porcentagem: {4}, sendo está superior a informada: {5} ",
                                                                produtoBase.DESEMPSRR, produtoBase.NUMIDTMERAPIMKP, produtoBase.VLRVNDMER, produtoHistorico.VLRVNDMER, diferencaPorcentagem, args),
                                                 "SJB119: Validando a diferença entre as tabelas em porcentagem", false);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}