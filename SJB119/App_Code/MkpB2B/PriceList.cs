﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace SJB119.App_Code.MkpB2B
{

    public class ProductMartins {
        public string DESEMPSRR { get; set; }
        public string NUMIDTMERAPIMKP { get; set; }
        public string VLRVNDMER { get; set; }
        public string DESLSTPCOENVFRN { get; set; }
    }

    public class ProductHistoricoMartins {
        public string NUMIDTEMPSRRAPIMKP { get; set; }
        public string VLRVNDMER { get; set; }
    }


    public class PriceList
    {
        public string price_list_code { get; set; }
        public string price_list_discription { get; set; }
        public List<Scope> scopes { get; set; }
    }

    public class Scope
    {
        public string code { get; set; }
        public string discription { get; set; }
    }


    public class Items
    {
        public int total { get; set; }
        public List<Product> products { get; set; }
    }
    public class Item
    {
        public long product_id { get; set; }
        public long seller_code { get; set; }
        public long variation_option_id { get; set; }
        public double sale_price { get; set; }
        public double list_price { get; set; }
        public string sku_seller_id { get; set; }
        public long product_seller_id { get; set; }
        public string protocol { get; set; }
        public string erp_code { get; set; }
    }

    public class Product
    {
        public long product_id { get; set; }
        public long seller_code { get; set; }
        public string erp_code { get; set; }
        public string bar_code { get; set; }
        public long variation_option_id { get; set; }
        public List<Price> prices { get; set; }
    }

    public class Price
    {
        public string marketplace_scope_code { get; set; }
        public double sale_price { get; set; }
        public double list_price { get; set; }
        public string protocol { get; set; }
        public string update_at { get; set; }
    }

}
