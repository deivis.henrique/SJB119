﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Web.Script.Serialization;
using SJB119.Util;

namespace SJB119.App_Code.MkpB2B
{
    /// <summary>
    /// Classe utilizada p/ comunicar com API B2B Service Marketplace;
    /// </summary>
    public class MplaceService
    {
        public Items GetListaPrecoItens(int _offset, int _limit, long seller_id)
        {
            Items objResponse = new Items();
            HttpWebResponse response = null;
            Stream dataStream = null;
            StreamReader sReader = null;

            try
            {
                string uri = string.Empty;
                string token = string.Empty;

                // Obtém Parâmetros de Comunicação com API;
                this.GetParametersAPI(ref token, ref uri, string.Format("/api/PriceList/Items?offset={0}&limit={1}&seller_id={2}", _offset, _limit, seller_id));

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(uri));

                request.Method = "GET";
                request.ContentType = "application/json";
                request.Headers.Add("access-token",token);
                //request.Timeout = 60000;
                response = (HttpWebResponse)request.GetResponse();

                sReader = new StreamReader(response.GetResponseStream());

                string strJson = sReader.ReadToEnd();

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new WebException(strJson);

                objResponse = new JavaScriptSerializer().Deserialize<Items>(strJson);
            }
            catch (WebException ex)
            {
                ////Throw ex
                string msgFalhaComunicacaoAPI = ex.Message.Trim();
                string erroComunicacaoAPI = " ### ERRO: " + ex.ToString();
                string msgResponse = string.Empty;
                if (ex.Response != null)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)ex.Response;
                    Stream dataResponse = ex.Response.GetResponseStream();
                    StreamReader readerResponse = new StreamReader(dataResponse);
                    msgResponse = readerResponse.ReadToEnd();
                    erroComunicacaoAPI += " ### RESPONSE: " + httpResponse.StatusCode + " - " + msgResponse;
                }

                // Envia email;
                Email email = new Email()
                {
                    Assunto = "SJB119 - Marketplace: Falha GetListaPrecoItens",
                    Remetente = ConfigurationManager.AppSettings.GetValues("Remetente").First(),
                    Mensagem = erroComunicacaoAPI.Trim(),
                    ConteudoHTML = false,
                    SmtpClient = ConfigurationManager.AppSettings.GetValues("Smtp").First()
                };
                email.Destinatarios.Add(ConfigurationManager.AppSettings.GetValues("DestinatarioTI").First());
                email.Send();
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response.Dispose();
                }

                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream.Dispose();
                }

                if (sReader != null)
                {
                    sReader.Close();
                    sReader.Dispose();
                }
            }
            return objResponse;
        }

        public void ProtocolaItens(List<string> list, string arg)
        {
            Log.GravaLog("      *** Protocolando Itens de Lista de Preço Openk Mplace", false, false, arg);

            HttpWebResponse response = null;
            Stream dataStream = null;
            StreamReader sReader = null;

            try
            {
                string objRequestSerialize;
                string uri = string.Empty;
                string token = string.Empty;

                // Obtém Parâmetros de Comunicação com API;
                this.GetParametersAPI(ref token, ref uri, "/api/PriceList/Items");

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(uri));

                request.Method = "PUT";
                request.ContentType = "application/json";
                request.Headers.Add("access-token", token);
                request.Timeout = 240000;

                if (list != null && list.Count > 0)
                {
                    objRequestSerialize = new JavaScriptSerializer().Serialize(list);

                    Log.GravaLog("             -> Protocolos: " + objRequestSerialize, false, false, arg);

                    byte[] byteArray = Encoding.UTF8.GetBytes(objRequestSerialize);

                    request.ContentLength = byteArray.Length;

                    dataStream = request.GetRequestStream();

                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }
                else
                    request.ContentLength = 0;

                response = (HttpWebResponse)request.GetResponse();

                sReader = new StreamReader(response.GetResponseStream());

                string strJson = sReader.ReadToEnd();

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new Exception(strJson);
            }
            catch (WebException ex)
            {
                ////Throw ex
                string msgFalhaComunicacaoAPI = ex.Message.Trim();
                string erroComunicacaoAPI = " ### ERRO: " + ex.ToString();
                string msgResponse = string.Empty;
                if (ex.Response != null)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)ex.Response;
                    Stream dataResponse = ex.Response.GetResponseStream();
                    StreamReader readerResponse = new StreamReader(dataResponse);
                    msgResponse = readerResponse.ReadToEnd();
                    erroComunicacaoAPI += " ### RESPONSE: " + httpResponse.StatusCode + " - " + msgResponse;
                }

                // Envia email;
                Email email = new Email()
                {
                    Assunto = "SJB119 - Marketplace: Falha ProtocolaItens",
                    Remetente = ConfigurationManager.AppSettings.GetValues("Remetente").First(),
                    Mensagem = erroComunicacaoAPI.Trim(),
                    ConteudoHTML = false,
                    SmtpClient = ConfigurationManager.AppSettings.GetValues("Smtp").First()
                };
                email.Destinatarios.Add(ConfigurationManager.AppSettings.GetValues("DestinatarioTI").First());
                email.Send();
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response.Dispose();
                }

                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream.Dispose();
                }

                if (sReader != null)
                {
                    sReader.Close();
                    sReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Obtém Parâmetros de Comunicação com API;
        /// </summary>
        /// <param name="accessToken">ref - accessToken</param>
        /// <param name="uri">ref - uri</param>
        /// <param name="complementoUri">complementoUri</param>
        private void GetParametersAPI(ref string accessToken, ref string uri, string complementoUri)
        {
            ExeConfigurationFileMap configExternoMap = new ExeConfigurationFileMap
            {
                ExeConfigFilename = ConfigurationManager.AppSettings.GetValues("Path_MPlace").First()
            };

            Configuration configExternoFile = ConfigurationManager.OpenMappedExeConfiguration(configExternoMap, ConfigurationUserLevel.None);

            // Obtém Parâmetros de Produção da Braspag;
            string urlApiMPlace = configExternoFile.AppSettings.Settings["UrlApiMPlace_PRODUCAO"].Value.Trim();
            accessToken = configExternoFile.AppSettings.Settings["AccessToken_PRODUCAO"].Value.Trim();

            // "Tombo" para Utilizar Parâmetros de Homologação;
            if (Dns.GetHostName().Trim().ToUpper() != "BETRIA")
            {
                urlApiMPlace = configExternoFile.AppSettings.Settings["UrlApiMPlace_HOMOLOGACAO"].Value.Trim();
                accessToken = configExternoFile.AppSettings.Settings["AccessToken_HOMOLOGACAO"].Value.Trim();
            }

            uri = urlApiMPlace + complementoUri;
        }
    }
}
